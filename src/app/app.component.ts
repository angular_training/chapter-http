import { Component } from '@angular/core';
import { TasksService } from './tasks.service';
import { Response } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  tasks = [
    {
      title: 'Learn Angular',
      progress: 60,
      id: this.generateId()
    },
    {
      title: 'Learn GraphQL',
      progress: 10,
      id: this.generateId()
    }
  ];

  appName = this.tasksService.getAppName();
  
  constructor(private tasksService: TasksService) {}

  onAddTask(title: string) {
    this.tasks.push({
       title: title,
       progress: 50,
       id: this.generateId()
    });
  }

  onSave() {
    this.tasksService.storeTasks(this.tasks).subscribe(
      response => console.log(response),
      error => console.log(error)
    );
  }

  getAllTasks() {
    // this.tasksService.getTasks().subscribe(
    //   (response: Response) => console.log(response.json()),
    //   error => console.log(error)
    // );

    this.tasksService.getTasks().subscribe(
      (tasks: any[]) => this.tasks = tasks,
      error => console.log(error)
    );
  }

  private generateId() {
    return Math.round(Math.random() * 10000);
  }
}
