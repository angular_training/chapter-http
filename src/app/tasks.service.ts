import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()
export class TasksService {

  backendServiceUrl = 'https://hps-http-backend.firebaseio.com/data';
  constructor(private http: Http) { }
  
  storeTasks(tasks: any[]) {
    // no request is sent while no one is subscribing to the 
    // observable returned by hhtp post method
    const reqHeaders = new Headers({'Content-Type': 'application/json'});
    return this.http.put(this.backendServiceUrl, tasks, { headers: reqHeaders});
  }

  getTasks() {
    return this.http.get(this.backendServiceUrl).pipe(map(
      (response: Response) => {
        const data = response.json();
        for(let task of data) {
          task.title += '_FETCHED'
        }
        return data;
      }
    ),
    catchError(err => {
      console.log('Error while invoking backend server..');
      return throwError('Error while invoking backend server..');
    }));
  }

  getAppName() {
    return this.http.get('https://hps-http-backend.firebaseio.com/data/appName.json').pipe(
      map((response: Response) => {
        return response.json();
      }),
      catchError((error) => {
        const err = 'error while getting app name from server';
        return  throwError(err);
      })
    )
  }
}
